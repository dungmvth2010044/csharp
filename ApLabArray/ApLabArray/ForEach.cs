﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ApLabArray
{
    class ForEach
    {
        static void Main(string[] args)
        {
            DateTime now = DateTime.Now;
            Random rand = new Random((int)now.Millisecond);
            int[] Arr = new int[10];
            for (int x = 0; x < Arr.Length; ++x)
            {
                Arr[x] = rand.Next() % 100;
            }
            int Total = 0;
            Write("Array values are ");
            foreach (int val in Arr)
            {
                Total += val;
                Write(val + ", ");
            }
            WriteLine("\nAnd the average is {0,0:F1}",
                (double)Total / (double)Arr.Length);
            ReadLine();
        }
    }
}
