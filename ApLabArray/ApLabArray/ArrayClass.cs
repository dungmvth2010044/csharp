﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
namespace ApLabArray
{
    class ArrayClass
    {
        static void Main(string[] args)
        {
            int[] Arr = new int[12] { 29, 82, 42, 46, 54, 65, 50, 42, 5, 94, 19, 34 };
            WriteLine("The first occurrence of 42 is at index" + Array.IndexOf(Arr, 42));
            WriteLine("The last occurrence of 42 is at index" + Array.LastIndexOf(Arr, 42));
            int x = 0;
            while ((x = Array.IndexOf(Arr, 42, x)) >= 0)
            {
                WriteLine("42 found at index " + x);
                ++x;
            }
            x = Arr.Length - 1;
            while ((x = Array.LastIndexOf(Arr, 42, x)) >= 0)
            {
                WriteLine("42 found at index " + x);
                --x;
            }
            WriteLine("Array that before sorted");
            for (int i = 0; i < Arr.Length; i++)
            {
                WriteLine("{0} :   {1}", i + 1, Arr[i]);
            }
            Array.Reverse(Arr);
            WriteLine("Array that after reserse");
            for (int i = 0; i < Arr.Length; i++)
            {
                WriteLine("{0} :       {1}", i + 1, Arr[i]);
            }
            ReadLine();
        }
    }
}
