﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Test_Client
{
    class BankAccount
    {
        public int id;
        public decimal balance;
        public int Id { get; set; }
        public decimal Balance { get; set; }
        public static void Create(string[] cmdArgs, Dictionary<int, BankAccount> accounts)
        {
            int id = int.Parse(cmdArgs[1]);
            if(accounts.ContainsKey(id))
            {
                Console.WriteLine("Account exists!!!");
            }
            else
            {
                BankAccount acc = new BankAccount();
                acc.id = id;
                accounts.Add(id, acc);
            }
        }
        public static void Deposit() { }
        public static void Withdraw() { }
        public static void Print(string[] cmdArgs, Dictionary<int, BankAccount> accounts) {
            int id = int.Parse(cmdArgs[1]);
            if (!accounts.ContainsKey(id))
            {
                Console.WriteLine("Account does not exist!!!");
            }
            else
            {
                Console.WriteLine($"")
            }
        }
    }
}
