﻿//Exercise 2: Write a program to accept three integer numbers and find the maximum number from three integers.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_4_Do_it_yourself
{
    class Exercise_2
    {
        static void Main(string[] args)
        {
            int a, b, c;

            Console.WriteLine("Enter a :");
            a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter b :");
            b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter c :");
            c = Convert.ToInt32(Console.ReadLine());

            if (a > b && a > c)
            {
                Console.WriteLine("Large number is : {0}", a);
            }
            else if (b > a && b > c)
            {
                Console.WriteLine("Large number is : {0}", b);
            }
            else if (c > a && c > b)
            {
                Console.WriteLine("Large number is : {0}", c);
            }
            else
            {
                Console.WriteLine("all three numbers are equal ");
            }

            Console.ReadKey();
        }
    }
}
