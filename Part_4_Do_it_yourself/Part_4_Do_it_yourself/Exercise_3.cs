﻿//Exercise 3: Write a program that accepts a number between 1 and 7 from the user and return the corresponding day of the week(1- Monday, 2- Tuesday and so on).
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_4_Do_it_yourself
{
    class Exercise_3
    {
        static void Main(string[] args)
        {
            int day;
            Console.WriteLine("Enter your lucky number (from 1 to 7) :");
            day = Convert.ToInt32(Console.ReadLine());

            switch (day)
            {
                case 1:
                    Console.WriteLine("Monday is your day !");
                    break;
                case 2:
                    Console.WriteLine("Tuesday is your day !");
                    break;
                case 3:
                    Console.WriteLine("Wednesday is your day !");
                    break;
                case 4:
                    Console.WriteLine("Thursday is your day !");
                    break;
                case 5:
                    Console.WriteLine("Friday is your day !");
                    break;
                case 6:
                    Console.WriteLine("Saturday is your day !");
                    break;
                case 7:
                    Console.WriteLine("Sunday is your day !");
                    break;
                default:
                    Console.WriteLine("There is no this day! ");
                    break;

            }
        }
    }
}
