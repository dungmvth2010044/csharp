﻿//Exercise 1: Write a program to enter: name, address, phone and display this information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_4_Do_it_yourself
{
    class Exercise_1
    {
        static void Main(string[] args)
        {
            string name;
            string address;
            string phone;

            Console.WriteLine("Enter Student Name :");
            name = Console.ReadLine();

            Console.WriteLine("Enter Student Address :");
            address = Console.ReadLine();

            Console.WriteLine("Enter Student Phone Number :");
            phone = Console.ReadLine();

            Console.WriteLine("\n");
            Console.WriteLine("==== Student Detail ====");
            Console.WriteLine("Name : " + name);
            Console.WriteLine("Address : " + address);
            Console.WriteLine("Phone number : " + phone);
        }
    }
}
