﻿//Exercise 4: Write a program to display the first 9 multiples of an integer. N entered from user
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_4_Do_it_yourself
{
    class Exercise_4
    {
        static void Main(string[] args)
        {
            int N;
            Console.Write("Import N :");
            N = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("=================The first multiple of 9 ==============\n");
            for (int i = 1; i <= 9; i++)
            {
                Console.WriteLine("\t\t\t {0} x {1} = {2} \n", N, i, N * i);
            }
        }
    }
}
