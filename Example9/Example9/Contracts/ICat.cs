﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example9.Contracts
{
    interface ICat : IFeline
    {
        string Breed { get; }
    }
}
