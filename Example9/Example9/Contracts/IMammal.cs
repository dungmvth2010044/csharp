﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example9.Contracts
{
    interface IMammal : IAnimal
    {
        string LivingRegion { get; }
    }
}
