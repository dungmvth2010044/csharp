﻿using Example9.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example9.Models
{
    public abstract class Animal : IAnimal
    {
        private string name;
        private double weight;
        private int foodEaten;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        public int FoodEaten
        {
            get { return foodEaten; }
            set { foodEaten = value; }
        }
        public Animal(string name, double weight)
        {
            Name = name;
            Weight = weight;
        }

        public virtual void Eat(IFood food)
        {
            FoodEaten += food.Quantity;

        }
        public virtual void ProduceSound()
        {
            Console.WriteLine("Producing Sound!!!");
        }
    }
}
