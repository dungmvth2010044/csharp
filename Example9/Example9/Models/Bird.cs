﻿using Example9.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example9.Models
{
    public abstract class Bird : Animal, IBird
    {
        //public double WingSize => throw new NotImplementedException();

        private double wingSize;
        public double WingSize;
        public double WingSize
        {
            get { return wingSize; }
            set { wingSize = value; }
        }
        public Bird(string name, double weight, double wingSize)
            :base(name, weight)
        {
            wingSize = wingSize;
        }
        public override void ProduceSound()
        {
            //base.ProduceSound();
        }
    }
}
