﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP_11_9
{
    class Product_Management
    {
        public List<Product> ListProduct = null;
        public Product_Management()
        {
            ListProduct = new List<Product>();
        }

        //Hàm tạo ID tăng dần cho câu hỏi
        public int GenerateID()
        {
            int max = 1;
            if (ListProduct != null && ListProduct.Count > 0)
            {
                max = ListProduct[0].STT;
                foreach (Product pr in ListProduct)
                {
                    if (max < pr.STT)
                    {
                        max = pr.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //hàm đếm số sản phẩm
        public int CountProduct()
        {
            int count = 0;
            if (ListProduct != null)
            {
                count = ListProduct.Count;
            }
            return count;
        }


        // Hàm thêm mới sản phẩm
        public void CreateProduct()
        {
            Product sp = new Product();
            sp.STT = GenerateID();

            Console.Write("Enter Product ID : ");
            sp.ProductID = Convert.ToString(Console.ReadLine());

            Console.Write("Enter Product Name : ");
            sp.Name = Convert.ToString(Console.ReadLine());

            Console.Write("Enter Product Price : ");
            sp.Price = Convert.ToDouble(Console.ReadLine());

            Console.Write("\n\n\t\t\tDo you want to save this Product ? (Y/N) : ");
            string save = Convert.ToString(Console.ReadLine());
            if (save == "Y" || save == "y" || save == "yes" || save == "YES")
            {
                Console.Clear();
                ListProduct.Add(sp);
                Console.WriteLine("\n\n\t\tAdd Product success !\n");
            }
            else if (save == "N" || save == "n" || save == "no" || save == "NO")
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t Product is not saved !\n\n");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t Auto break Create Product !\n\n");
            }
        }
        //Hàm tìm kiếm theo mã SP
        public Product FindByProductID(string id)
        {
            Product searchID = null;
            if (ListProduct != null && ListProduct.Count > 0)
            {
                foreach (Product pro in ListProduct)
                {
                    if (pro.ProductID == id)
                    {
                        searchID = pro;
                    }
                }
            }
            return searchID;
        }

        //hàm xóa Product theo ID
        public bool DeleteByID(string id)
        {
            bool IsDeleted = false;
            Product prID = FindByProductID(id);
            if (prID != null)
            {
                IsDeleted = ListProduct.Remove(prID);
            }
            return IsDeleted;
        }

        // Hàm hiển thị danh sách Product
        public void ShowProduct(List<Product> products)
        {

            if (products != null && products.Count > 0)
            {
                foreach (Product sp in products)
                {
                    Console.WriteLine("\nNo : {0} \n ProductID : {1} \n   Product Name : {2} \n Price : {3}", sp.STT, sp.ProductID, sp.Name, sp.Price);

                }
            }
            Console.WriteLine();
        }

        //Hàm trả về danh sách Product
        public List<Product> GetProducts()
        {
            return ListProduct;
        }

    }
}

