﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace One_line_only
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j, k;
            Console.Write("Moi ban nhap i :");
            i = Convert.ToInt32(Console.ReadLine());

            Console.Write("Moi ban nhap j :");
            j = Int32.Parse(Console.ReadLine());

            Console.Write("Moi ban nhap k :");
            k = Int32.Parse(Console.ReadLine());

            if ((i != j && j != k) || (i == j && j != k) || (i != j && j == k) || (i == k && j != k))
            {
                int max = i > j ? i : j;

                max = max > k ? max : k;
                Console.WriteLine("So lon nhat la : {0}", max);
            }
            else
            {
                Console.WriteLine("Ba so bang nhau");
            }
            Console.ReadLine();
        }
    }
}
