﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example06
{
    public class SelectionSort
    {
        static void selectSort(int []array)
        {
            int n = array.Length;//n bằng độ dài của array
            for (int i = 0; i < n - 1; i++) 
            {
                int min_indx = i;//Tim phan tu nho nhat
                for (int j = i + 1; j < n; j++)
                {
                    if (array[j] < array[min_indx])
                    {
                        int temp = array[min_indx];
                        array[min_indx] = array[i];
                        array[i] = temp;
                    }
                }
            }
        }
    }
}
