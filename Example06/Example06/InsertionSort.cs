﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example06
{
    class InsertionSort
    {
        public void insertSort(int[] array)
        {
            int n = array.Length;
            for(int i = 1; i < n; ++i)
            {
                int key = array[i];//gt cua phan tu thu 2
                int j = i - 1;// vi tri phan tu dau tien(j = 0)
                while(j >= 0 && array [j] > key)//dk true
                    {
                    array[j + 1] = array[j];
                    //j = j - 1;
                    j--;//tra ve gia tri dau tien
                }
                array[j + 1] = key;//chen
            }
        }
    }
}
