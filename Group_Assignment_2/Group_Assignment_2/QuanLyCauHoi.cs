﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group_Assignment_2
{
    class QuanLyCauHoi
    {
        private List<_1_3_Add_a_new_Q_A> ListCauHoi = null;
        public QuanLyCauHoi()
        {
            ListCauHoi = new List<_1_3_Add_a_new_Q_A>();
        }

        //Hàm trả về số lượng hiện tại trong ListCauHoi
        public int SoLuongCauHoi()
        {
            int Count = 0;
            if (ListCauHoi != null)
            {
                Count = ListCauHoi.Count;
            }
            return Count;
        }

        public void Create_a_new_Q_A()
        {
            // Tạo mới một Q/A.
            _1_3_Add_a_new_Q_A add = new _1_3_Add_a_new_Q_A();

            Console.Write("Nhập mã danh mục : ");
            add.Catalog_Code = Convert.ToString(Console.ReadLine());

            Console.Write("Đây là lĩnh vực : ");
            add.Field = Convert.ToString(Console.ReadLine());

            Console.Write("Nhập mã câu hỏi : ");
            add.Question_Code = Convert.ToString(Console.ReadLine());

            Console.Write("Câu hỏi : ");
            add.Question = Convert.ToString(Console.ReadLine());

            Console.Write("Điểm số : ");
            add.Scores = Convert.ToDouble(Console.ReadLine());

            Console.Write("Đáp án 1 : ");
            add.Answer_1 = Convert.ToString(Console.ReadLine());
            Console.Write("Đúng/Sai?(D/S) : ");
            add.D_S_1 = Convert.ToString(Console.ReadLine());

            Console.Write("Đáp án 2 : ");
            add.Answer_2 = Convert.ToString(Console.ReadLine());

            Console.Write("Đúng/Sai?(D/S) : ");
            add.D_S_2 = Convert.ToString(Console.ReadLine());
            

            Console.Write("Đáp án 3  : ");
            add.Answer_3 = Convert.ToString(Console.ReadLine());

            Console.Write("Đúng/Sai?(D/S) : ");
            add.D_S_3 = Convert.ToString(Console.ReadLine());

 
            ListCauHoi.Add(add);

        }

        /**
         * Hàm tìm kiếm câu hỏi theo mã câu hỏi
         * Trả về câu hỏi trong mục kiến thức phổ thông 
         */
        public List<_1_3_Add_a_new_Q_A> FindBy_Question_Code(String keyword)
        {
            List<_1_3_Add_a_new_Q_A> searchResult = new List<_1_3_Add_a_new_Q_A>();
            if (ListCauHoi != null && ListCauHoi.Count > 0)
            {
                foreach (_1_3_Add_a_new_Q_A add in ListCauHoi)
                {
                    if (add.Question_Code.ToUpper().Contains(keyword.ToUpper()))
                    {
                        searchResult.Add(add);
                    }
                }
            }
            return searchResult;
        }
        
         // Hàm hiển thị danh sách câu hỏi

        public void ShowCauHoi(List<_1_3_Add_a_new_Q_A> listCH)
        {
            if (listCH != null && listCH.Count > 0)
            {
                foreach (_1_3_Add_a_new_Q_A add in listCH)
                {
                    Console.WriteLine("======Kiến thức phổ thông========");
                    Console.WriteLine("\nMã danh mục : {0} \n Đây là lĩnh vực : {1} \n" +
                        " Mã câu hỏi : {2} \n Câu hỏi : {3} \n Điểm số : {4} \n Đáp án 1 : {5}" +
                        "Đáp án 2 : {6} \n Đúng/Sai?(D/S) :{7}\n Đáp án 3 :{8}\n Đúng/Sai?(D/S){9}",
                                      add.Catalog_Code , add.Field, add.Question_Code, add.Question, add.Scores
                                      , add.Answer_1, add.D_S_1, add.Answer_2, add.D_S_2,add.Answer_3, add.D_S_3);
                }
            }
            Console.WriteLine();
        }

    }
}
