﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Group_Assignment_2
{
    class Program
    {
        static void Main(string[] args)
        {
            QuanLyCauHoi quanLyCauHoi = new QuanLyCauHoi();

            // Menu chính
            int userInput;
            int result1;
            do
            {
                userInput = mainMenu();
                switch (userInput)
                {
                    case 0:
                        Clear();
                        Console.WriteLine("\n Goodbye !! \n");
                        Environment.Exit(0);
                        break;
                    case 1:
                        Clear();
                        Manage_Questions_Answers();
                        Exam_Management();

                        break;
                    case 2:
                        Clear();
                        Exam_Management();
                        break;

                    default:
                        Clear();
                        WriteLine("\t\nKhông hợp lệ! Vui lòng nhập lại !!!!\n");
                        break;


                }
            }
            
            while (userInput != 3);

            //1. Quản lý Câu hỏi/Trả lời
            do
            {
                result1 = Manage_Questions_Answers();
                switch (result1)
                {
                    case 0:
                        Clear();
                        mainMenu();
                        break;
                    case 1:
                        Clear();
                        List_of_questions();
                        ComeBackMenu();
                        break;
                    case 2:
                        Clear();
                        ComeBackMenu();
                        break;
                    case 3:
                        Clear();
                        quanLyCauHoi.Create_a_new_Q_A();
                        ComeBackMenu();
                        break;
                    default:
                        Clear();
                        WriteLine("\t\nKhông hợp lệ! Vui lòng nhập lại !!!!\n");
                        break;
                }
            }
            while (result1 != 4);

            //1.1 Danh sách các câu hỏi
            do
            {
                result1 = List_of_questions();
                switch (result1)
                {
                    case 0:
                        Clear();
                        Manage_Questions_Answers();
                        break;
                    case 1:
                        Clear();
                        if (quanLyCauHoi.SoLuongCauHoi() > 0)
                        {
                            Console.WriteLine("======Kiến thức phổ thông========");
                            Console.WriteLine("Kiến thức phổ thông có tổng cộng 130 câu.");
                            Console.Write("Bạn muốn xem câu hỏi nào? (Nhập mã câu hỏi) :");
                            string Question_Code = Convert.ToString(Console.ReadLine());
                            List<_1_3_Add_a_new_Q_A> searchResult = quanLyCauHoi.FindBy_Question_Code(Question_Code);
                            quanLyCauHoi.ShowCauHoi(searchResult);
                        }
                        else
                        {
                            Console.WriteLine("\n Danh sách câu hỏi trống!");
                        }
                        ComeBackMenu();
                        break;
                    case 2:
                        Clear();
                        ComeBackMenu();
                        break;
                    case 3:
                        Clear();

                        ComeBackMenu();
                        break;
                    default:
                        Clear();
                        WriteLine("\t\nKhông hợp lệ! Vui lòng nhập lại !!!!\n");
                        break;
                }
            }
            while (result1 != 4);






            //2. Quản lý đề thi
            do
            {
                result1 = Exam_Management();
                switch (result1)
                {
                    case 0:
                        Clear();
                        mainMenu();
                        break;
                    case 1:
                        Clear();
                        ComeBackMenu();
                        break;
                    case 2:
                        Clear();
                        ComeBackMenu();
                        break;
                    default:
                        Clear();
                        WriteLine("\t\nKhông hợp lệ! Vui lòng nhập lại !!!!\n");
                        break;
                }
            }
            while (result1 != 3);
        }





        private static void ComeBackMenu()
        {
            WriteLine("Nhấn phím bất kỳ để tiếp tuc chương trình.");
            ReadLine();
            Clear();
        }
        private static int mainMenu()
        {
            WriteLine("\n" +
                "--Chương trình quản lý đề thi----\n");
            WriteLine("=================================");
            WriteLine();
            WriteLine("1. Quản lý câu hỏi / trả lời.");
            WriteLine("2. Quản lý đề thi.");
            WriteLine("0.Thoát");

            Write("\nMời bạn nhập từ (0-2) :\n ");

            var result1 = ReadLine();
            return string.IsNullOrEmpty(result1) ? 0 : Convert.ToInt32(result1);

        }

        //1. Quản lý Câu hỏi/Trả lời
        public static int Manage_Questions_Answers()
        {
            WriteLine("======Quản lý Câu hỏi/Trả lời=======");
            WriteLine();
            WriteLine("1. Xem danh sách Q/A.");
            WriteLine("2. Cập nhật Q/A.");
            WriteLine("3. Tạo mới một Q/A.");
            WriteLine("0. Trở về menu chính.");

            Write("\nMời bạn nhập từ (0-3) :\n ");

            var result1_1 = ReadLine();
            return string.IsNullOrEmpty(result1_1) ? 0 : Convert.ToInt32(result1_1);
        }
        //1.1

        public static int List_of_questions()
        {
            WriteLine("======Danh sách các câu hỏi=========");
            WriteLine();
            WriteLine("1. Kiến thức phổ thông.");
            WriteLine("2. C Programming.");
            WriteLine("3. Lịch sử.");
            WriteLine("0. Trờ về menu trước.");

            Write("\nMời bạn nhập từ (0-3) :\n ");

            var result1_1 = ReadLine();
            return string.IsNullOrEmpty(result1_1) ? 0 : Convert.ToInt32(result1_1);
        }

        //2. Quản lý đề thi.
        public static int Exam_Management()
        {
            WriteLine("====Quản lý đề thi=================");
            WriteLine();
            WriteLine("1. Xem đề thi");
            WriteLine("2. Tạo đề thi");
            WriteLine("0. Trở về menu chính.");

            Write("\nMời bạn nhập từ (0-2) :\n ");

            var result2 = ReadLine();
            return string.IsNullOrEmpty(result2) ? 0 : Convert.ToInt32(result2);
        }
        
    }
}