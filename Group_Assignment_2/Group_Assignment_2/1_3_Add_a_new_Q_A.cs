﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group_Assignment_2
{
    class _1_3_Add_a_new_Q_A
    {
        public string Catalog_Code { get; set; }//Mã danh mục
        public string Field { get; set; }//Lĩnh vực
        public string Question_Code { get; set; } //mã câu hỏi
        public string Question { get; set; }//Câu hỏi
        public double Scores { get; set; }//điểm số
        public string Answer_1 { get; set; }//đáp án
        public string Answer_2 { get; set; }
        public string Answer_3 { get; set; }

        public string D_S_1 { get; set; }//đúng sai
        public string D_S_2 { get; set; }
        public string D_S_3 { get; set; }

    }
}
