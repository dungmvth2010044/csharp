﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string studentName;
            int age;
            string gender;
            string branch = "Information Technology ";
            string courses = "programmer ";

            Console.Write("Enter Student Name : ");
            studentName = Console.ReadLine();

            Console.Write("Enter Student Age :");
            age = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter gender : ");
            gender = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine(" ============= Your details ==========\n");
            Console.WriteLine("Name :" + studentName);
            Console.WriteLine("Age :" + age);
            Console.WriteLine("Gender :" + gender);
            Console.WriteLine("Branch :" + branch);
            Console.WriteLine("Courses :" + courses);
            Console.ReadKey(false);
        }
    }
}
