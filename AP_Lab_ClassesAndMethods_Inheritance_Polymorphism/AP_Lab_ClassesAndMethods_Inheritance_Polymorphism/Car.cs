﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP_Lab_ClassesAndMethods_Inheritance_Polymorphism
{
    class Car
    {

        // declare the fields
        public string make;
        public string model;
        public string color;
        public int yearBuilt;
        // define the methods
        public void Start()
        {
            System.Console.WriteLine(model + "started");
        }
        public void Stop()
        {
            System.Console.WriteLine(model + " stopped");
        }
    }

    /*class Program
    {
        public static void Main()
        {
            // declare a Car object reference nameD myCar
            Car myCar;
            // create a Car object, and assign its address to myCar
            System.Console.WriteLine("Creating a Car object and assigning "
                + "its memory location to myCar");
            myCar = new Car();

            // assign values to the Car object's fields using myCar
            myCar.make = "Toyota";
            myCar.model = "MR2";
            myCar.color = "black";
            myCar.yearBuilt = 1995;

            // display the field values using myCar
            System.Console.WriteLine("myCar details:");
            System.Console.WriteLine("myCar.make = " + myCar.make);
            System.Console.WriteLine("myCar.model = " + myCar.model);
            System.Console.WriteLine("myCar.color = " +myCar.color);
            System.Console.WriteLine("myCar.yearBuilt = " + myCar.yearBuilt);

            // call the methods using myCar
            myCar.Start();
            myCar.Stop();

            // declare another Car object reference and
            // create another Car object
            System.Console.WriteLine("Creating another Car object and" + "assigning its memory location to redPorsche");
            Car redPorsche = new Car();
            redPorsche.make = "Porche";
            redPorsche.model = "Boxster";
            redPorsche.color = "red";
            redPorsche.yearBuilt = 2000;
            System.Console.WriteLine("redPorsche is a " + redPorsche.model);
            //change the object referenced by the myCar object 
            //reference to the object referenced by redPorsche
            System.Console.WriteLine("Assigning redPorsche to myCar");
            myCar = redPorsche;
            System.Console.WriteLine("myCar details:");

            System.Console.WriteLine("myCar.make ="+ myCar.make);

            System.Console.WriteLine("myCar.model = " + myCar.model);

            System.Console.WriteLine("myCar.color = " + myCar.color);

            System.Console.WriteLine("myCar.yearBuilt = "+ myCar.yearBuilt);

            //assign null to myCar (myCar will no longer reference
            //an object
            myCar = null;
            Console.WriteLine();
        }
    }*/
    class Window
    {
        //constructor takes two integers to
        //fix location on the console
        public Window(int top, int left)
        {
            this.top = top;
            this.left = left;
        }
        //simulates drawing the window
        public virtual void DrawWindow()
        {
            Console.WriteLine("Window: drawing Window at {0}, {1}",top, left);
        }
        // these members are protected and thus visible
        //to derived class methods. We'll examine this
        //later in the chapter
        protected int top;
        protected int left;
    }

    class ListBox : Window
    {
        //constructor adds a parameter
        public ListBox(int top, int left, string contents):base(top, left)//call base constructor
        {
            ListBoxContents = contents;
        }
        // an overridden version (note keyword) because in the
        // derived method we change the behavior
        public override void DrawWindow()
        {
            base.DrawWindow(); // invoke the base method
            Console.WriteLine("Writing string to the listbox: {0}", ListBoxContents);
        }
        private string ListBoxContents;
    }
    class Button : Window
    {
        public Button(int top, int left): base(top, left)
        {
        }
        //an overridden version (note keyword) because in the
        //derived method we change the behavior
        public override void DrawWindow()
        {
            Console.WriteLine("Drawing a button at {0}, {1}\n", top, left);
        }
    }
    class Polymorphism
    {
        public static void Main(string[] args)
        {
            Window win = new Window(1, 2);
            ListBox lb = new ListBox(3, 4, "Stand alone list box");
            Button b = new Button(5, 6);
            win.DrawWindow();
            lb.DrawWindow();
            b.DrawWindow();
            Window[] winArray = new Window[3];
            winArray[0] = new Window(1, 2);
            winArray[1] = new ListBox(3, 4, "List box in array");
            winArray[2] = new Button(5, 6);
            for (int i = 0; i < 3; i++)
            {
                winArray[i].DrawWindow();
            }
            Console.ReadLine();
        }
    }
}
