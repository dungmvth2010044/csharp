﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Ferrari
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter name :");
                string driverName = Console.ReadLine();
                Ferrari ferrari = new Ferrari(driverName);
                ferrari.GasPedal();
                ferrari.Brakes();
                Console.WriteLine($"{ferrari.Model} /{ferrari.Brakes()} /{ferrari.GasPedal()} /{ferrari.Driver}\n");
            }
        }
    }
}
