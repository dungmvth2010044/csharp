﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Ferrari
{
    public interface IFerrari
    {
        string Model { get; }
        string Driver { get; }
        string Brakes();
        string GasPedal();
    }
}