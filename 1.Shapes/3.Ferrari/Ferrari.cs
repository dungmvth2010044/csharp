﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Ferrari
{
    public class Ferrari : IFerrari
    {
        public string Model { get; set; } = "488-Spider";
        public string Driver { get; set; }
        public Ferrari(string driver)
        {
            Driver = driver;
        }
        public string GasPedal()
        {
            return "Zadu6avam";
        }
        public string Brakes()
        {
            return "Brakes!";
        }
    }
}