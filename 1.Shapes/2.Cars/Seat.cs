﻿namespace _2.Cars
{
    class Seat : ICar
    {
        public Seat(string model, string color)
        {
            this.Model = model;
            this.Color = color;
        }
        public string Model { get; set; }

        public string Color { get; set; }

        public string Start()
        {
            return "Engine Start";
        }

        public string Stop()
        {
            return "Breaaak!!!";
        }
        public override string ToString()
        {
            return $"{Color} Seat {Model} \n {Start()} \n {Stop()} \n";
        }
    }
}
