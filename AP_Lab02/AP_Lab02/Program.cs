﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace AP_Lab02
{
    class Program
    {
        static void Main(string[] args)
        {

            // Menu chính
            int userInput;
            int result1;
            do
            {
                userInput = mainMenu();
                switch (userInput)
                {
                    case 0:
                        Clear();
                        Console.WriteLine("\n Goodbye !! \n");
                        Environment.Exit(0);
                        break;
                    case 1:
                        Clear();
                        manageTeamList();
                        ComeBackMenu();
                        break;
                    case 2:
                        Clear();
                        manageSchedule();
                        ComeBackMenu();
                        break;
                    case 3:
                        Clear();
                        manageResult();
                        ComeBackMenu();
                        break;
                    case 4:
                        Clear();
                        resultStatistics();
                        ComeBackMenu();
                        break;
                    default:
                        Clear();
                        WriteLine("\nKhông hợp lệ! Vui lòng nhập lại !\n");
                        break;

                }
            }
            while (userInput != 5);

            //1. Quản lý danh sách đội bóng
            do
            {
                result1 = manageTeamList();
                switch (result1)
                {
                    case 0:
                        Clear();
                        mainMenu();
                        break;
                    case 1:
                        Clear();
                        ComeBackMenu();
                        break;
                    case 2:
                        Clear();
                        ComeBackMenu();
                        break;
                    case 3:
                        Clear();

                        ComeBackMenu();
                        break;
                }
            }
            while (result1 != 4);

        }
           

        private static void ComeBackMenu()
        {
            WriteLine("Nhấn phím bất kỳ để tiếp tuc chương trình.");
            ReadLine();
            Clear();
        }
        private static int mainMenu()
        {
            WriteLine("--Chào mừng đến với V-League 2021-----\n");
            WriteLine("=================================");
            WriteLine();
            WriteLine("1. Quản lý danh sách đội bóng.");
            WriteLine("2. Quản lý lịch thi đấu.");
            WriteLine("3. Quản lý kết quả thi đấu.");
            WriteLine("4. Thống kê.");
            WriteLine("0.Thoát");

            Write("\nMời bạn nhập từ (0-4) :\n ");

            var result = ReadLine();
            return string.IsNullOrEmpty(result) ? 0 : Convert.ToInt32(result);

    }

        //1. Quản lý danh sách đội bóng.
        private static int manageTeamList()
        {
            WriteLine("=================================");
            WriteLine();
            WriteLine("1. Xem danh sách đội bóng.");
            WriteLine("2. Cập nhật danh sách đội bóng.");
            WriteLine("3. Thêm mới một đội bóng.");
            WriteLine("0. Trở về menu chính.");

            Write("\nMời bạn nhập từ (0-3) :\n ");

            var result1 = ReadLine();
            return string.IsNullOrEmpty(result1) ? 0 : Convert.ToInt32(result1);
        }
        //2. Quản lý lịch thi đấu.
        private static int manageSchedule()
        {
            WriteLine("=================================");
            WriteLine();
            WriteLine("1. Xem lịch thi đấu");
            WriteLine("2. Cập nhật cập nhật lịch thi đấu");
            WriteLine("3. Tạo lịch thi đấu");
            WriteLine("0. Trở về menu chính.");

            Write("\nMời bạn nhập từ (0-3) :\n ");

            var result2 = ReadLine();
            return string.IsNullOrEmpty(result2) ? 0 : Convert.ToInt32(result2);
        }
        //3. Quản lý kết quả thi đấu.
        private static int manageResult()
        {
            WriteLine("=========Kết quả thi đấu===========");
            WriteLine();
            WriteLine("1. Thể Công – Viettel 3 - 0 Hoàng Anh Gia Lai");
            WriteLine("2. SLNA vs Hải Phòng");
            WriteLine("0. Trở về menu chính.");

            Write("\nMời bạn nhập từ (0-2) :\n ");

            var result3 = ReadLine();
            return string.IsNullOrEmpty(result3) ? 0 : Convert.ToInt32(result3);
        }

        //4. Thống kê.
        private static int resultStatistics()
        {
            WriteLine("=================================================================");
WriteLine("|\tMã đội\t|\tTên đội\t|\tTrận\t|\tThắng\t|\tHòa\t|\tThua\t|\tĐiểm\t|\n");
            WriteLine("=================================================================");
            WriteLine("|\tTC08\t|\tThể công- Viettel\t|\t20\t|\t10\t|\t2\t|\t0\t|\t32\t|");
            WriteLine("\t|\tHAGL\t|\tHoàng Anh Gia Lai\t|\t19 \t |\t  9 \t |\t 4 \t| \t 1  \t | \t31   \t|");
WriteLine("=================================================================");

            WriteLine(" Chúc các đội giải V-League thành công!");

            var result4 = ReadLine();
            return string.IsNullOrEmpty(result4) ? 0 : Convert.ToInt32(result4);
        }
    }
}
