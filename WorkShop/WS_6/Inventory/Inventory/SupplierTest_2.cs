﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    /// <summary>
    /// Class SupplierTest is used to instantiate the Supplier class.
    /// </summary>
    class SupplierTest_2
    {
        /// <summary>
        /// The entry poin for the application.
        /// </summary>
        /// <param name="args">A list of command line arguments</param>
        static void Main(string[] args)
        {
            // Creating an object of the Supplier class
            Supplier objSupplier = new Supplier();

            // Invoking the AcceptDetails method of the Supplier class
            // to accept the details of the supplier
            objSupplier.AcceptDetails();

            // Integer variable to accept the id of the supplier
            int id = 0;

            // String varianle toaccept the name of the supplier
            string name = "";

            // Accepting the id of the supplier
            Console.Write("\nEnter the id of the supplier : ");
            id = Convert.ToInt32(Console.ReadLine());

            // Invoking the DisplayDetails method of the Supplier class
            // to display the details of the supplier by passing id as a parameter
            objSupplier.DisplayDetails(id);

            // Accepting the name of the supplier
            Console.WriteLine("\nEnter the name of the supplier :");
            name = Console.ReadLine();

            //Invoking the DisplayDetails method of the Supplier class
            // to display the details of the supplier by passing name as a parameter
            objSupplier.DisplayDetails(name);

            // Invoking the DisplayDetails method of the Supplier class
            // to display the city of the supplier by passing id and name as paraments
            objSupplier.DisplayDetails(id, name);
        }
    }
}