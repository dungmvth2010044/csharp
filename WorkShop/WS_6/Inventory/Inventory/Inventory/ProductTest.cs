﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class ProductTest
    {
        /// <summary>
        /// The entry point for the application.
        /// </summary>
        /// <param name="args">A list of command line Arguments</param>
        static void Main(string[]args)
        {
            //Creating an object of the Product class and
            //invoking the defauls constructor without parameters
            Product objProduct = new Product();

            //Invoking the DisplyDetails method of the Product class
            //to display the details of the product
            objProduct.DisplayDetails();
            Console.WriteLine();

            // Creating an object of the Product class and
            // invoking a parameterised constructor with a single parameter
            Product objProduct1 = new Product(102);
            //Invoking the DisplayDetails method of the Product class
            // to display the details of the product
            objProduct1.DisplayDetails();
            Console.WriteLine();

            //Creating an object of the Product class and 
            // invoking a parameterised contructor with four parameters
            Product objProduct2 = new Product(103, "Television", 5660.45F, 68);

            // Invoking the DisplayDetails method of the Product class
            // to display the details of the product
            objProduct2.DisplayDetails();

        }
    }
}
