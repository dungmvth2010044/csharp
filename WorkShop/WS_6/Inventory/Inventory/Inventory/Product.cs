﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    /// <summary>
    /// 
    /// The program demonstrates the use of overloading a constructor.
    /// 
    /// Class Product creates three constructors to initialise the details of the
    /// product and a method to display them.
    /// </summary>
    class Product
    {
        /// <summary>
        /// 
        /// String field to store the ID of the product.
        /// </summary>
        private int _productID;

        /// <summary>
        /// String field tostore the name of the product.
        /// </summary>
        private string _productName;

        /// <summary>
        /// Float field to store the price of the product.
        /// </summary>

        private float _price;

        /// <summary>
        /// iNTEGER FIELD TO STORE THE STOCK OF THE PRODUCT.
        /// </summary>
        /// 
        private int _stock;

        /// <summary>
        /// Constructor without parameters to initialise
        /// details of the product.
        /// </summary>
        
        public Product()
        {
            _productID = 101;
            _productName = "Refrigerator";
            _price = 420.5F;
            _stock = 30;
        }

        /// <summary>
        /// Constructor with a single parameter to initialise
        /// details of the product.
        /// </summary>
        /// <param name="id">Accepts id of the product</param>
        public Product(int id)
        {
            _productID = id;
            _productName = "Washing Machine";
            _price = 677.3F;
            _stock = 25;
        }
        /// <summary>
        /// Constructor with four parameters to initialise
        /// details of the product.
        /// </summary>
        /// <param name ="id">Accepts id of the product</param>
        /// <param name ="name">Accepts name of the product</param>
        /// <param name ="price">Accepts price of the product</param>
        /// <param name ="stock">Accepts stock of the product</param>
        public Product(int id, string name, float price, int stock)
        {
            _productID = id;
            _productName = name;
            _price = price;
            _stock = stock;
        }
        /// <summary>
        /// Method to display the details of the product.
        /// </summary>
        public void DisplayDetails()
        {
            Console.WriteLine("Product Details :");
            Console.WriteLine("Product ID : " + _productID);
            Console.WriteLine("Product Name : " + _productName);
            Console.WriteLine("Price : " + _price + " $");
            Console.WriteLine("Quantity in Stock : " + _stock);
        }
    }
}
