﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    /// <summary>
    /// 
    /// The program demonstrates the use of creating a class
    /// 
    /// Class Suuplier accer accepts and display the details of the supplier
    /// </summary>
    class Supplier
    {
        /// <summary>
        /// 
        /// Integer field tostore the ID of the supplier.
        /// </summary>
        
        private int _supplierID;
        /// <summary>
        /// 
        /// String field to store the city of the supplier.
        /// </summary>

        private string _supplierName;

        /// <summary>
        /// 
        /// String field to store the city of the supplier.
        /// </summary>

        private string _city;
        /// <summary>
        /// 
        /// String field to store the phone number of the supplier.
        /// </summary>

        private string _phoneNo;

        /// <summary>
        /// 
        /// String field to store the email address of the supplier.
        /// </summary>

        private string _email;

        /// <summary>
        /// 
        /// String field to store the email address of the supplier.
        /// </summary>

        public void AccaptDetails()
        {
            Console.Write("Enter the ID of supplier : ");
            _supplierID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the name of supplier : ");
            _supplierName = Console.ReadLine();

            Console.Write("Enter the name of city : ");
            _city = Console.ReadLine();

            Console.Write("Enter phone No. : ");
            _phoneNo = Console.ReadLine();

            Console.Write("Enter email address : ");
            _email = Console.ReadLine();
        }

        /// <summary>
        /// Method to display the details of the supplier.
        /// </summary>
        public void DisplayDetails()
        {
            Console.WriteLine("\nSupplier Details :");
            Console.WriteLine("Supplier ID : " + _supplierID);
            Console.WriteLine("Supplier Name : " + _supplierName);
            Console.WriteLine("City : " + _city);
            Console.WriteLine("Phone No : " + _phoneNo);
            Console.WriteLine("Email :" + _email);
        }
    }
}
