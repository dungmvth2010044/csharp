﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    /// <summary>
    /// Class SupplierTest is used to instantiate the Supplier class.
    /// </summary>
    class SupplierTest
    {
        static void Main(string[] args)
        {
            // Creating an object of the Supplier class
            Supplier objSupplier = new Supplier();

            // Invoking the AcceptDetails method of the Supplier class
            // to accept the details of the Supplier
            objSupplier.AccaptDetails();

            // Invoking the DisplayDetails method of the Supplier class
            // to display the details of the Supplier
            objSupplier.DisplayDetails();
        }
    }
}
