﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// 
    /// The program demonstrates the use of multidimensional array.
    /// 
    /// Class Books accepts and display the details of books using multidimensional array.
    /// </summary>
    class Books_2
    {
        /// <summary>
        /// 
        /// The entry poin for the application.
        /// </summary>
        /// <param name="args"> A list of command line arguments</param>
        static void Main(string[] args)
        {
            // Array of string to store column names
            string[] colName = new string[4] { "Book Title", "Author", "Publisher", "Price($)" };

            // Two dimensional array of string type to store book details
            string[,] bookDetails = new string[2, 4];

            // Accepting and storing the detals of books
            Console.WriteLine("Enter book details :\n");

            for (int i = 0 ; i < bookDetails.GetLength(0); i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("{0} : ", colName[j]);
                    bookDetails[i, j] = Console.ReadLine();
                }
                Console.WriteLine();
            }

            // Displaying the column names
            Console.WriteLine("Details of books : \n");
            foreach (string names in colName)
            {
                Console.Write("{0}\t\t", names);
            }
            Console.WriteLine();

            //Displaying the details of books
            for (int i = 0; i < bookDetails.GetLength(0); i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(bookDetails[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
