﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company
{
    /// <summary>
    /// 
    /// The program demonstrates the use of typecasting.
    /// 
    /// Class Employee accepts and displays the details of the employee and
    /// calculates the salary.
    /// </summary>
    class Employee
    {
        /// <summary>
        /// The entry poin for the application.
        /// </summary>
        /// <param name="args">A list of commad line arguments</param>
        static void Main(string[] args)
        {
            // Variables ddecared to store employee details
            int employeeID;
            string employeeName;
            string designation;
            float taxAmount = 0;
            double salary = 0, netSalary = 0;

            //Accepting the employee details using explicit typecasting
            Console.Write("Enter the id of an employee : ");
            employeeID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the name of emloyee : ");
            employeeName = Console.ReadLine();

            Console.Write("Enter designation : ");
            designation = Console.ReadLine();

            Console.Write("Enter salary : ");
            salary = Convert.ToDouble(Console.ReadLine());

             // Calculating tax and net salary based on basic salary
             // using explicit and implicit typecasting
             if (salary >= 10000)
            {
                taxAmount = (float)(salary * 32.5 / 100);
            }
             else
            {
                taxAmount = (float)(salary * 24.8 / 100);
            }
            netSalary = salary - taxAmount;

            // Displaying the details of the employee using explictit typecasting
            Console.WriteLine("\nEmployee Details:");
            Console.WriteLine("Employee ID : " + employeeID);
            Console.WriteLine("Emloyee Name : " + employeeName);
            Console.WriteLine("Designation : " + designation);
            Console.WriteLine("Salary : {0} $", salary);
            Console.WriteLine("Net salary : {0:F2} $ is rounded off to : " + netSalary + "$",netSalary);
        }
    }
}
