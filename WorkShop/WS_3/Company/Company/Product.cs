﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company
{
    /// <summary>
    /// The program demonstrates theuse of boxing and unboxing.
    /// 
    /// Class Product accepts the details od the product, converts the details
    /// into their relevant types using unboxing.
    /// </summary>
    class Product
    {
        /// <summary>
        /// The entry point for the application.
        /// </summary>
        /// <param name="args"> A list of commad line arguments</param>
        static void Main(string[] args)
        {
            //Creating objects of Object class to store the
            // details of the product
            Object objProductID;
            Object objProductName;
            Object objPrice;
            Object objQuantity;

            // Accepting and converting the details of the products
            // into reference types using implicit boxing

            Console.Write("Enter the ID of Product :");
            objProductID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the name of product :");
            objProductName = Console.ReadLine();

            Console.Write("Enter price :");
            objPrice = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter quantity :");
            objQuantity = Convert.ToInt32(Console.ReadLine());


            // Converting objects into their relevant types
            // using unboxing

            int productID = (int)objProductID;
            string productName = (string)objProductName;
            double price = (double)objQuantity;
            int quantity = (int)objQuantity;
            double amtPayable = (int)objQuantity * price;

            // Displaying the details of the product
            Console.WriteLine("\nProduct Details :");
            Console.WriteLine("Product ID :" + productID);
            Console.WriteLine("Product Name : " + productName);
            Console.WriteLine("Price :" + price);
            Console.WriteLine("Quantity :" + quantity);
            Console.WriteLine("Amt Payable {0:F2} :", amtPayable);


        }
    }
}
