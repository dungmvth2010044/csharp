﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Trường không gian tên chứa lớp XMLComment và phương thức Main 
namespace School
{
    /// <summary>
    /// Chương trình thể hiện việc sử dụng chú thích XML
    /// 
    /// -> Thẻ <summary> được sử dụng để cung cấp thông tin về Chương trình
    /// 
    /// Class XMLComments chứa các nhận xét ở định dạng XML. 
    /// </summary>
    class XMLComments
    {
        /// <summary>
        /// <c>Main</c> method là điểm đầu vào cho một ứng dụng C # 
        /// 
        /// <param name="="args">Danh sách các đối số dòng lệnh có thể được cung cấp
        /// using <c>args</c></param>
        /// <returns>kiểu trả về của phương thức Main là Void </returns>
        /// </summary>
        /// <remarks>
        /// Phương thức Main có thể được khai báo có hoặc không có tham số
        /// </remarks>
        /*thẻ <param> mô tả tham số cho một phương thức
         * thẻ <returns> mô tả giá trị trả về cho phương thức
         */

        static void Main(string[] args)
        {
            /*
             *Phương thức writeLine được sử dụng để in
             *Phương thức writeLine được sử dụng trong lớp Console trong
              *Không gian tên hệ thống. 
             */
            Console.WriteLine("This program illustrates XML Comments");
        }
    }
}
