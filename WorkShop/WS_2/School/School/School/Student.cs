﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    /// <summary>
    /// Student Class lưu trữ và hiển thị thông tin chi tiết của sinh viên
    /// sử dụng các kiểu dữ liệu khác nhau. 
    /// </summary>

    class Student
    {
        /// <summary>
        /// Điểm đầu vào cho ứng dụng 
        /// </summary>
        /// <param name="args"></param> Danh sách các đối số dòng lệnh </param> 
        static void Main(string[] args)
        {
          

            /*
             * //khai báo và khởi tạo các biến để lưu trữ thông tin chi tiết về sinh viên 
             int studentID = 1; 
             string studentName = "David George";
             byte age = 18;
             char gender = 'M';
             float percent = 75.50F; // Lưu trữ phần trăm của Student
             bool pass = true;  // bool dùng để lưu trữ kết quả của sinh viên

            //hiển thị thông tin chi tiết về sinh viên 
             Console.WriteLine("Student ID : {0}", studentID);
             Console.WriteLine("Student Name : {0}", studentName);
             Console.WriteLine("Age : " + age);
             Console.WriteLine("Gender : " + gender);
             Console.WriteLine("Percentage : {0:F2}", percent); //F2 cho biết số thập phân mong muốn
             Console.WriteLine("Passed : {0}", pass);

             */

            const int percentConst = 100;
            string studentName;
            int english, maths, science;
            float percent = 0.0F;

            Console.Write("Enter name of the student :");
            studentName = Console.ReadLine();

            Console.Write("Enter mark for english :");
            english = Convert.ToInt32(Console.ReadLine());
            /*Câu lệnh Console.Write hiển thị . Phương thức Console.ReadLine() sẽ chấp nhận các điểm của học sinh
             * chuyển nó thành kiểu dữ liệu int và lưu vào biến english*/

            Console.Write("Enter mark for maths :");
            maths = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter marks for science :");
            science = Convert.ToInt32(Console.ReadLine());

            percent = ((english + maths + science) * percentConst) / 300;

            Console.WriteLine();
            Console.WriteLine("*** Student Details ***\n");
            Console.WriteLine("Student Name :" + studentName);
            Console.WriteLine("Marks obtained in English : {0}", english);
            Console.WriteLine("Marks obtained in Maths : {0}", maths);
            Console.WriteLine("Marks obtained in Science : {0}", science);
            Console.WriteLine("Percent : {0:F2}", percent);
        }
    }
}
